# Goal
The repo has just one Dockerfile with ultra light weighted image. `FROM alpine:latest` which is `~ 5Mo` (in order not to overlord the registry storage)  
This procedure is intended to be followed after a maintenance on __gitlab__ / __registry__.

# Automatic build & push (prefered)
Run the CI !  
https://gitlab.com/baptiste-dauphin/registry-tester/-/pipelines

# Manually (fall back)

## Test push
To test the push. Clone on your laptop the repo. Then build + push
```bash
git clone git@gitlab.com:baptiste-dauphin/registry-tester.git /tmp/registry-tester

cd /tmp/registry-tester


docker login --username yourmail@domain.com registry.gitlab.com

docker build -t registry.gitlab.com/baptiste-dauphin/registry-tester/alpine_scratch .
docker push registry.gitlab.com/baptiste-dauphin/registry-tester/alpine_scratch
```

## Test pull
remove all not-running image
```bash
docker image prune --all

docker pull registry.gitlab.com/baptiste-dauphin/registry-tester/alpine_scratch
```
